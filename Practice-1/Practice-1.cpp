#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <iostream>

int main()
{
	int k = 0, n1 = 0, n2 = 0;
	char buffer[65], buffer_1[65];
	char numbers[65];
	char *pEnd;
	char numbers_1[65];
	int  result = 0;

	setlocale(LC_ALL, "Rus");

	while (true)
	{
		printf("Введите систему счисления:");
		scanf_s("%d", &k);
		if (k < 2 || k > 36)
		{
			printf("Неверная система счисления!\nПопробуйте снова\n");
		}
		else
		{
			printf("Введите число:");
			scanf_s("%s", numbers, (unsigned)_countof(numbers));
			printf("Введите второе число:");
			scanf_s("%s", numbers_1, (unsigned)_countof(numbers_1));

			int li3 = strtol(numbers, &pEnd, k);
			int li4 = strtol(numbers_1, &pEnd, k);

			if (li3 != 0 || li4 != 0)
			{
				_itoa_s(li3, buffer, 65, 10);
				_itoa_s(li4, buffer_1, 65, 10);
				result = li3 % li4;
				printf("Результат в десятичной системе счисления:\nОстаток от деления:%d\nПервое число:%d\nВторое число:%d\n", result, li3, (li4-4));
			}
			break;
		}
	}
		return 0;
}